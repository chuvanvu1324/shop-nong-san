@extends('auth.master')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-6 order-md-2">
                <img src="https://preview.colorlib.com/theme/bootstrap/login-form-08/images/undraw_file_sync_ot38.svg"
                    alt="Image" class="img-fluid">
            </div>
            <div class="col-md-6 contents">
                <div class="row justify-content-center">
                    @if (session('status') && session('message'))
                    <div class="alert alert-{{session('status')}} alert-dismissible fade show" role="alert">
                        {{session('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="col-md-8">
                        <div class="mb-4">
                            <h3>Đăng ký</h3>
                            {{-- <p class="mb-4">Lorem ipsum dolor sit amet elit. Sapiente sit aut eos consectetur
                                adipisicing.</p> --}}
                        </div>
                        <form action="{{ route('user.postRegister') }}" method="POST">
                            @csrf
                            <div class="form-group first">
                                <label>Họ & tên</label>
                                <input type="text" name="name" id="name" value="{{ old('name') }}" placeholder="Họ & tên" class="form-control" required>
                            </div>
                            
                            <div class="form-group first">
                                <label>Email</label>
                                <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="{{ __('voyager::generic.email') }}" class="form-control" required>
                            </div>

                            <div class="form-group last mb-4">
                                <label for="password">Mật khẩu</label>
                                <input type="password" name="password" placeholder="Mật khẩu" class="form-control" required>
                            </div>
                            
                            <div class="form-group last mb-4">
                                <label for="address">Địa chỉ</label>
                                <input type="text" name="address" placeholder="Địa chỉ" class="form-control" required>
                            </div>
                            
                            <div class="form-group last mb-4">
                                <label for="address">Số điện thoại</label>
                                <input type="text" name="phone" placeholder="Số điện thoại" class="form-control" required>
                            </div>


  
                            <input type="submit" value="Đăng ký" class="btn text-white btn-block btn-primary">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
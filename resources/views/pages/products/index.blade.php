@extends('master')

@section('content')
@include('layouts.breadcrumb', [
'title' => 'Cửa hàng',
'breadcrumbs' => [
['name'=> 'Trang chủ', 'route' => 'home'],
['name'=> 'Cửa hàng', 'route' => 'products.index'],
]
])

<section class="product spad">
    <div class="container">
        <div class="row">
            @if (session('status') && session('message'))
            <div class="col-sm-12">
                <div class="alert alert-{{session('status')}} alert-dismissible fade show" role="alert">
                    {{session('message')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            @endif
            <div class="col-lg-3 col-md-5">
                <div class="sidebar">
                    <div class="sidebar__item">
                        <h4>Danh mục</h4>
                        <ul>
                            @foreach ($categories as $category_menu)
                            <li>
                                <a
                                    href="{{route('products.category', ['id' => $category_menu->id, 'slug' => $category_menu->slug])}}">{{$category_menu->name}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="sidebar__item">
                        <div class="latest-product__text">
                            <h4>Nông sản mới</h4>
                            <div class="latest-product__slider owl-carousel owl-loaded owl-drag">
                                @for ($i = 0; $i < count($products); $i+=3) @php $p1=isset($products[$i]) ? $products[$i] :null; $p2=isset($products[$i+1]) ? $products[$i+1] :null;
                            $p3=isset($products[$i+2]) ? $products[$i+2] :null; @endphp <div class="latest-prdouct__slider__item">
                            @if($p1)
                            <a href="{{route('products.detail', [$p1->id, $p1->slug])}}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{Storage::url($p1->image)}}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{$p1->title}}</h6>
                                    <span>{{number_format($p1->price, 0, ',', '.')}}đ</span>
                                </div>
                            </a>
                            @endif
                            @if($p2)
                            <a href="{{route('products.detail', [$p2->id, $p2->slug])}}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{Storage::url($p2->image)}}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{$p2->title}}</h6>
                                    <span>{{number_format($p2->price, 0, ',', '.')}}đ</span>
                                </div>
                            </a>
                            @endif
                            @if($p3)
                            <a href="{{route('products.detail', [$p3->id, $p3->slug])}}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{Storage::url($p3->image)}}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{$p3->title}}</h6>
                                    <span>{{number_format($p3->price, 0, ',', '.')}}đ</span>
                                </div>
                            </a>
                            @endif
                    </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-7">
            <div class="product__discount">
                <div class="section-title product__discount__title">
                    <h2>Giảm giá</h2>
                </div>
                <div class="row">
                    <div class="product__discount__slider owl-carousel owl-loaded owl-drag">
                        @foreach ($saleOffs as $item)
                        <div class="col-lg-4">
                            <div class="product__discount__item">
                                <div class="product__discount__item__pic set-bg"
                                    data-setbg="{{Storage::url($item->image)}}">
                                    @php
                                    $sale = $item->price - $item->old_price;
                                    $sale = ($sale/$item->old_price)*100;
                                    $sale = round($sale);
                                    @endphp
                                    <div class="product__discount__percent">{{$sale}}%</div>
                                    <ul class="product__item__pic__hover">
                                        {{-- <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fa fa-retweet"></i></a></li> --}}
                                        <li><a href="{{route('carts.get.store', [$item->id])}}"><i
                                                    class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="product__discount__item__text">
                                    <span>Dried Fruit</span>
                                    <h5><a
                                            href="{{route('products.detail', [$item->id, $item->slug])}}">{{$item->title}}</a>
                                    </h5>
                                    <div class="product__item__price">{{number_format($item->price, 0, ',', '.')}}đ
                                        <span>{{number_format($item->old_price, 0, ',', '.')}}đ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="filter__item">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="filter__sort">
                            <span>Sắp xếp</span>
                            <select style="display: none;">
                                <option value="asc">Tăng dần</option>
                                <option value="desc">Giảm dần</option>
                            </select>
                            <div class="nice-select" tabindex="0"><span class="current">Default</span>
                                <ul class="list">
                                    <li data-value="0" class="option selected">Default</li>
                                    <li data-value="0" class="option">Default</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        {{-- <div class="filter__found">
                                <h6><span>16</span> Products found</h6>
                            </div> --}}
                    </div>
                    <div class="col-lg-4 col-md-3">
                        <div class="filter__option">
                            <span class="icon_grid-2x2"></span>
                            <span class="icon_ul"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($products as $item)
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="{{Storage::url($item->image)}}">
                            <ul class="product__item__pic__hover">
                                {{-- <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li> --}}
                                <li><a href="{{route('carts.get.store', [$item->id])}}"><i
                                            class="fa fa-shopping-cart"></i></a></li>
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="{{route('products.detail', [$item->id, $item->slug])}}">{{$item->title}}</a>
                            </h6>
                            <h5>{{number_format($item->price, 0, ',', '.')}}</h5>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{$products->render()}}
            {{-- <div class="product__pagination">
                    <a href="#">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                </div> --}}
        </div>
    </div>
    </div>
</section>
@endsection
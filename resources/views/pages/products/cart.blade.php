@extends('master')

@section('content')
@include('layouts.breadcrumb', [
'title' => 'Giỏ hàng',
'breadcrumbs' => [
['name'=> 'Trang chủ', 'route' => 'home'],
['name'=> 'Giỏ hàng', 'route' => 'products.cart'],
]
])
<section class="shoping-cart spad">
    <form action="{{route('carts.update')}}" method="post">
        @csrf
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Sản phẩm</th>
                                    <th>Giá</th>
                                    <th>Số lượng</th>
                                    <th>Tổng tiền</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (Cart::count() > 0)
                                @foreach (Cart::content()->values() as $item)
                                <tr>
                                    <td class="shoping__cart__item">
                                        <img data-setbg="{{Storage::url($item->options['image'])}}" alt="">
                                        <h5>{{$item->name}}</h5>
                                    </td>
                                    <td class="shoping__cart__price">{{number_format($item->price, 0, ',', '.')}}đ</td>
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                            <div class="pro-qty">
                                                {{-- <span class="dec qtybtn">-</span> --}}
                                                <input type="text" class="input-qty" name="{{$item->rowId}}"
                                                    value="{{$item->qty}}">
                                                {{-- <span class="inc qtybtn">+</span> --}}
                                            </div>
                                        </div>
                                    </td>
                                    <td class="shoping__cart__total">
                                        {{number_format($item->total, 0, ',', '.')}}đ
                                    </td>
                                    <td class="shoping__cart__item__close">
                                        <a href="{{route('carts.destroy', $item->rowId)}}">
                                            <span class="icon_close"></span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td class="text-center" colspan="4">
                                        <h5>Giỏ hàng của bạn đang trống</h5>
                                    </td>
                                </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row" id="cart_footer">
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="{{route('products.index')}}" class="primary-btn cart-btn">Tiếp tục mua hàng</a>
                        
                        <button type="submit" class="primary-btn cart-btn cart-btn-right">
                            {{-- <span class="icon_loading spinner fa-spinner" style=""></span> --}}
                            Cập nhật

                        </button>
                    </div>
                </div>
                <div class="col-lg-6"></div>
                <div class="col-lg-6">
                    @if (Cart::count() > 0)
                    <div class="shoping__checkout">
                        <h5>Tổng</h5>
                        <ul>
                            <li>Tổng đơn hàng <span>{{Cart::subtotal()}}đ</span></li>
                            <li>Tổng thanh toán <span>{{Cart::total()}}đ</span></li>
                        </ul>
                        <a href="{{route('checkout.index')}}" class="primary-btn">Thanh toán</a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </form>
</section>
@endsection
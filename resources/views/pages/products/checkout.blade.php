@extends('master')

@section('content')
@include('layouts.breadcrumb', [
'title' => 'Thanh Toán',
'breadcrumbs' => [
['name'=> 'Trang chủ', 'route' => 'home'],
['name'=> 'Thanh Toán', 'route' => 'checkout.index'],
]
])
<section class="checkout spad">
    <div class="container">
        {{-- <div class="row">
            <div class="col-lg-12">
                <h6><span class="icon_tag_alt"></span> Have a coupon? <a href="#">Click here</a> to enter your code
                </h6>
            </div>
        </div> --}}
        <div class="checkout__form">
            <h4>Chi tiết thanh toán</h4>
            <form action="{{route('checkout.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="checkout__input">
                                    <p>Họ & tên<span>*</span></p>
                                    <input type="text" name="name" value="{{old('name', Auth::check() ? Auth::user()->name : '')}}" required>
                                </div>
                            </div>

                        </div>

                        <div class="checkout__input">
                            <p>Địa chỉ<span>*</span></p>
                            <input type="text" name="address" value="{{old('address', Auth::check() ? Auth::user()->getMoreInfo('address') : '')}}" required class="checkout__input__add">
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>Số điện thoại<span>*</span></p>
                                    <input type="text" name="phone" value="{{old('phone', Auth::check() ? Auth::user()->getMoreInfo('phone') : '')}}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>Email<span>*</span></p>
                                    <input type="email" name="email" value="{{old('email', Auth::check() ? Auth::user()->email : '')}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="checkout__input">
                                    <p>Ghi chú</p>
                                    <textarea name="note" class="form-control" cols="30">{{old('note', '')}}</textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="checkout__order">
                            <h4>Đơn hàng của bạn</h4>
                            <div class="checkout__order__products">Sản phẩm <span>Tổng</span></div>
                            <ul>
                                @foreach (Cart::content()->values() as $item)
                                <li>{{$item->name}} <span>{{number_format($item->price, 0, ',', '.')}}đ</span></li>
                                @endforeach
                            </ul>
                            <div class="checkout__order__total">Tổng tiền <span>{{Cart::total()}}đ</span></div>

                            <div class="checkout__input__checkbox">
                                <label for="cod">
                                    Thanh toán khi nhận hàng
                                    <input type="checkbox" id="cod" checked>
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <button type="submit" class="site-btn">Đặt hàng</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@extends('master')

@section('content')
@include('layouts.breadcrumb', [
'title' => $product->title,
'breadcrumbs' => [
['name'=> 'Trang chủ', 'route' => 'home'],
['name'=> $product->title, 'route' => 'products.index'],
]
])
<section class="product-details spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="product__details__pic">
                    <div class="product__details__pic__item">
                        <img class="product__details__pic__item--large" src="{{Storage::url($product->image)}}" alt="">
                    </div>
                    <div class="product__details__pic__slider owl-carousel owl-loaded owl-drag">

                        <div class="owl-stage-outer">
                            <div class="owl-stage"
                                style="transform: translate3d(-1006px, 0px, 0px); transition: all 1.2s ease 0s; width: 1725px;">
                                <div class="owl-item cloned" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-2.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-1.jpg"
                                        alt=""></div>
                                <div class="owl-item cloned" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-3.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-2.jpg"
                                        alt=""></div>
                                <div class="owl-item cloned" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-5.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-3.jpg"
                                        alt=""></div>
                                <div class="owl-item cloned" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-4.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-4.jpg"
                                        alt=""></div>
                                <div class="owl-item" style="width: 123.75px; margin-right: 20px;">
                                    <img data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-2.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-1.jpg"
                                        alt="">
                                </div>
                                <div class="owl-item" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-3.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-2.jpg"
                                        alt=""></div>
                                <div class="owl-item" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-5.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-3.jpg"
                                        alt=""></div>
                                <div class="owl-item active" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-4.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-4.jpg"
                                        alt=""></div>
                                <div class="owl-item cloned active" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-2.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-1.jpg"
                                        alt=""></div>
                                <div class="owl-item cloned active" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-3.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-2.jpg"
                                        alt=""></div>
                                <div class="owl-item cloned active" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-5.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-3.jpg"
                                        alt=""></div>
                                <div class="owl-item cloned" style="width: 123.75px; margin-right: 20px;"><img
                                        data-imgbigurl="https://preview.colorlib.com/theme/ogani/img/product/details/product-details-4.jpg"
                                        src="https://preview.colorlib.com/theme/ogani/img/product/details/thumb-4.jpg"
                                        alt=""></div>
                            </div>
                        </div>
                        <div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><span
                                    aria-label="Previous">‹</span></button><button type="button" role="presentation"
                                class="owl-next"><span aria-label="Next">›</span></button></div>
                        <div class="owl-dots disabled"><button role="button"
                                class="owl-dot active"><span></span></button></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product__details__text">
                    <h3>{{$product->title}}</h3>
                    <div class="product__details__rating">
                        @for ($i = 0; $i < $product->rate; $i++)

                            <i class="fa fa-star"></i>
                            @endfor

                            @for ($i = $product->rate; $i < 5; $i++) <i class="fa fa-star-o"></i>
                                @endfor

                                {{-- <span>(18 reviews)</span> --}}
                    </div>
                    <div class="product__details__price">{{number_format($product->price, 0, ',', '.')}}đ</div>
                    <p>{{$product->excerpt}}</p>
                    <form action="{{route('carts.store')}}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{$product->id}}">
                        <div class="product__details__quantity">
                            <div class="quantity">
                                <div class="pro-qty">
                                    <input type="text" value="1" name="qty">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="primary-btn">Thêm giỏ hàng</button>
                    </form>
                    {{-- <a href="#" class="heart-icon"><span class="icon_heart_alt"></span></a> --}}
                    <ul>
                        <li><b>Trạng thái</b> <span>{{$product->status == 'instock' ? 'Còn hàng' : 'Hết hàng'}}</span>
                        </li>
                        <li><b>Vận chuyển</b> <span><samp>Miễn phí vận chuyển trong ngày hôm nay</samp></span></li>
                        {{-- <li><b>Weight</b> <span>0.5 kg</span></li> --}}
                        <li><b>Chia sẻ</b>
                            <div class="share">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="product__details__tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                aria-selected="true">Mô tả</a>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"
                                aria-selected="false">Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab"
                                aria-selected="false">Reviews <span>(1)</span></a>
                        </li> --}}
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="product__details__tab__desc">
                                {!!$product->body!!}
                            </div>
                        </div>
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <h6>Products Infomation</h6>
                                <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                    Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                    Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                    sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                    eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                    Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                    sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                    diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                    ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                    Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                    Proin eget tortor risus.</p>
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                    ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                    elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                    porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                    nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabs-3" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <h6>Products Infomation</h6>
                                <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                    Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                    Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                    sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                    eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                    Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                    sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                    diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                    ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                    Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                    Proin eget tortor risus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="related-product">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title related__product__title">
                    <h2>Sản phẩm liên quan</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($relatedProducts as $item)
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="product__item">
                    <div class="product__item__pic set-bg"
                        data-setbg="{{Storage::url($item->image)}}">
                        <ul class="product__item__pic__hover">
                            {{-- <li><a href="#"><i class="fa fa-heart"></i></a></li>
                            <li><a href="#"><i class="fa fa-retweet"></i></a></li> --}}
                            <li><a href="{{route('carts.get.store', [$item->id])}}"><i class="fa fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>
                    <div class="product__item__text">
                        <h6><a href="{{route('products.detail', [$item->id, $item->slug])}}">{{$item->title}}</a></h6>
                        <h5>{{number_format($item->price, 0, ',', '.')}}đ</h5>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
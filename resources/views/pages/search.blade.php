@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="text-center mb-3">Kết quả tìm kiếm cho: {{$_GET['s']}}</h2>
        </div>
        <div class="col-sm-12">
            <h3>Sản phẩm</h3>
            <ul class="list-group list-group-flush">
                @foreach ($products as $item)
                <li class="list-group-item">
                    <a href="{{route('products.detail', [$item->id, $item->slug])}}">{{$item->title}}</a>
                </li>
                @endforeach

            </ul>

            <h3 class="mt-5">Bài viết</h3>
            <ul class="list-group list-group-flush">
                @foreach ($posts as $item)
                <li class="list-group-item">
                    <a href="{{route('posts.detail', [$item->id, $item->slug])}}">{{$item->title}}</a>
                </li>
                @endforeach

            </ul>

        </div>
    </div>

</div>
@endsection
@extends('master')

@section('content')
<section class="blog-details-hero set-bg" data-setbg="https://preview.colorlib.com/theme/ogani/img/blog/details/details-hero.jpg"
    style="background-image: url(&quot;https://preview.colorlib.com/theme/ogani/img/blog/details/details-hero.jpg&quot;);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="blog__details__hero__text">
                    <h2>{{$post->title}}</h2>
                    <ul>
                        <li>Bởi admin</li>
                        {{-- <li>January 14, 2019</li>
                        <li>8 Comments</li> --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-details spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5 order-md-1 order-2">
                @include('layouts.blog-sidebar')
            </div>
            <div class="col-lg-8 col-md-7 order-md-1 order-1">
                <div class="blog__details__text">
                    <img src="{{Storage::url($post->image)}}" alt="">
                    {!! $post->body !!}
                </div>
                {{-- <div class="blog__details__content">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="blog__details__author">
                                <div class="blog__details__author__pic">
                                    <img src="https://preview.colorlib.com/theme/ogani/img/blog/details/details-author.jpg" alt="">
                                </div>
                                <div class="blog__details__author__text">
                                    <h6>Michael Scofield</h6>
                                    <span>Admin</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="blog__details__widget">
                                <ul>
                                    <li><span>Categories:</span> Food</li>
                                    <li><span>Tags:</span> All, Trending, Cooking, Healthy Food, Life Style</li>
                                </ul>
                                <div class="blog__details__social">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                    <a href="#"><i class="fa fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</section>
<section class="related-blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title related-blog-title">
                    <h2>Bài viết liên quan</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($relatedPosts as $item)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog__item">
                    <div class="blog__item__pic">
                        <img src="https://preview.colorlib.com/theme/ogani/img/blog/blog-1.jpg" alt="">
                    </div>
                    <div class="blog__item__text">
                        {{-- <ul>
                            <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                            <li><i class="fa fa-comment-o"></i> 5</li>
                        </ul> --}}
                        <h5><a href="{{route('posts.detail', [$item->id, $item->slug])}}">{{$item->title}}</a></h5>
                        <p>{{$item->excerpt}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
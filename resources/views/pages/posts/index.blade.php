@extends('master')

@section('content')
@include('layouts.breadcrumb', [
'title' => isset($cat) ? $cat->name : 'Tin tức',
'breadcrumbs' => [
['name'=> 'Trang chủ', 'route' => 'home'],
['name'=> isset($cat) ? $cat->name : 'Tin tức', 'route' => 'posts.index'],
]
])
<section class="blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5">
                @include('layouts.blog-sidebar')
            </div>
            <div class="col-lg-8 col-md-7">
                <div class="row">
                    @foreach ($posts as $item)
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="{{Storage::url($item->image)}}" alt="">
                            </div>
                            <div class="blog__item__text">
                                {{-- <ul>
                                    <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                                    <li><i class="fa fa-comment-o"></i> 5</li>
                                </ul> --}}
                                <h5><a href="{{route('posts.detail', [$item->id, $item->slug])}}">{{$item->title}}</a></h5>
                                <p>{{$item->excerpt}}</p>
                                <a href="{{route('posts.detail', [$item->id, $item->slug])}}" class="blog__btn">Xem thêm<span class="arrow_right"></span></a>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <div class="col-lg-12">
                        {!! $posts->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@extends('master')

@section('content')
<div class="container">
    <div class="row">
        @if (session('status') && session('message'))
        <div class="col-sm-12">
            <div class="alert alert-{{session('status')}} alert-dismissible fade show" role="alert">
                {{session('message')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
    </div>
    <div class="row mb-2">
        @include('layouts.menu-profile')
        <div class="col-sm-8">
            <form action="{{route('user.update')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="checkout__input">
                                    <p>Họ & tên<span>*</span></p>
                                    <input type="text" name="name"
                                        value="{{old('name', Auth::check() ? Auth::user()->name : '')}}" required>
                                </div>
                            </div>

                        </div>

                        <div class="checkout__input">
                            <p>Địa chỉ<span>*</span></p>
                            <input type="text" name="address"
                                value="{{old('address', Auth::check() ? Auth::user()->getMoreInfo('address') : '')}}"
                                required class="checkout__input__add">
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>Số điện thoại<span>*</span></p>
                                    <input type="text" name="phone"
                                        value="{{old('phone', Auth::check() ? Auth::user()->getMoreInfo('phone') : '')}}"
                                        required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>Email<span>*</span></p>
                                    <input type="email" value="{{old('email', Auth::check() ? Auth::user()->email : '')}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="checkout__input">
                                    <button class="btn btn-success">Cập nhật</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
    @php
    // dd($orders);
    @endphp
</div>
@endsection
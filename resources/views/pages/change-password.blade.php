@extends('master')

@section('content')
<div class="container">
    <div class="row">
        @if (session('status') && session('message'))
        <div class="col-sm-12">
            <div class="alert alert-{{session('status')}} alert-dismissible fade show" role="alert">
                {{session('message')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
    </div>
    <div class="row mb-2">
        @include('layouts.menu-profile')
        <div class="col-sm-8">
            <form action="{{route('user.update')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="checkout__input">
                                    <p>Mật khẩu cũ<span>*</span></p>
                                    <input type="text" name="old_pass"
                                        value="{{old('old_pass', '')}}" required>
                                </div>
                            </div>

                        </div>

                        <div class="checkout__input">
                            <p>Mật khẩu mới<span>*</span></p>
                            <input type="text" name="pass"
                                value="{{old('pass', '')}}"
                                required class="checkout__input__add">
                        </div>
                        
                        <div class="checkout__input">
                            <p>Mật khẩu mới<span>*</span></p>
                            <input type="text" name="re_pass"
                                value="{{old('re_pass', '')}}"
                                required class="checkout__input__add">
                        </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="checkout__input">
                                    <button class="btn btn-success">Đổi mật khẩu</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
    @php
    // dd($orders);
    @endphp
</div>
@endsection
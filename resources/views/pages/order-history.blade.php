@extends('master')

@section('content')
<div class="container">
    <div class="row">
        @if (session('status') && session('message'))
        <div class="col-sm-12">
            <div class="alert alert-{{session('status')}} alert-dismissible fade show" role="alert">
                {{session('message')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
    </div>
    <div class="row mb-2">
        @include('layouts.menu-profile')
        <div class="col-sm-8">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Mã đơn hàng</th>
                        <th scope="col">Số lượnng</th>
                        <th scope="col">Thành tiền</th>
                        <th scope="col">Ngày mua</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($orders as $item)
                    <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>{{$item->id}}</td>
                        <td>{{$item->qty}}</td>
                        <td>{{number_format($item->total, 0, ',', '.')}}đ</td>
                        <td>{{$item->created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @php
    // dd($orders);
    @endphp
</div>
@endsection
<section class="breadcrumb-section set-bg" data-setbg="https://preview.colorlib.com/theme/ogani/img/breadcrumb.jpg"
    style="background-image: url(&quot;https://preview.colorlib.com/theme/ogani/img/breadcrumb.jpg&quot;);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>{{$title}}</h2>
                    <div class="breadcrumb__option">
                        @foreach ($breadcrumbs as $b)
                        @if ($loop->last)
                        <span>{{$b['name']}}</span>
                        @else
                        <a href="{{route($b['route'])}}">{{$b['name']}}</a>
                        @endif
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
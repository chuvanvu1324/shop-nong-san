<div class="blog__sidebar">
    <div class="blog__sidebar__item">
        <h4>Danh mục</h4>
        <ul>
            <li><a href="{{route('posts.index')}}">Tất cả</a></li>
            @foreach ($categories as $item)
            <li><a href="{{route('posts.category', [$item->id, $item->slug])}}">{{$item->name}}</a></li>
            @endforeach
            
        </ul>
    </div>
    <div class="blog__sidebar__item">
        <h4>Bài viết gần đây</h4>
        <div class="blog__sidebar__recent">
            @foreach ($recentPost as $item)
            <a href="{{route('posts.detail', [$item->id, $item->slug])}}" class="blog__sidebar__recent__item">
                <div class="blog__sidebar__recent__item__pic">
                    <img src="{{Storage::url($item->image)}}" alt="">
                </div>
                <div class="blog__sidebar__recent__item__text">
                    <h6>{{$item->title}}</h6>
                    <span>{{$item->created_at}}</span>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</div>
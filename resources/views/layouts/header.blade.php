@php
$currentRouteName = Route::currentRouteName();
use App\Models\Category;
$categories = Category::product()->get();
@endphp
<div id="preloder" style="display: none;">
    <div class="loader" style="display: none;"></div>
</div>

<div class="humberger__menu__overlay"></div>
<div class="humberger__menu__wrapper">
    <div class="humberger__menu__logo">
        <a href="#"><img src="https://preview.colorlib.com/theme/ogani/img/logo.png" alt=""></a>
    </div>
    <div class="humberger__menu__cart">
        <ul>
            {{-- <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li> --}}
            <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
        </ul>
        <div class="header__cart__price">Thành tiền: <span>150.000đ</span></div>
    </div>
    {{-- <div class="humberger__menu__widget">
        <div class="header__top__right__language">
            <img src="https://preview.colorlib.com/theme/ogani/img/language.png" alt="">
            <div>English</div>
            <span class="arrow_carrot-down"></span>
            <ul>
                <li><a href="#">Spanis</a></li>
                <li><a href="#">English</a></li>
            </ul>
        </div>
        <div class="header__top__right__auth">
            <a href="#"><i class="fa fa-user"></i> Login</a>
        </div>
    </div> --}}
    <nav class="humberger__menu__nav mobile-menu">
        <ul>
            <li class="{{ $currentRouteName == 'home' ? 'active' : '' }}"><a href="{{ route('home') }}">Trang
                    chủ</a>
            </li>
            <li><a href="{{ route('products.index') }}">Cửa hàng</a></li>
            {{-- <li><a href="#">Pages</a>
                <ul class="header__menu__dropdown">
                    <li><a href="{{route('products.detail', ['id' => 1, 'slug' => 'product-example'])}}">Shop
            Details</a></li>
            <li><a href="{{route('products.cart')}}">Shoping Cart</a></li>
            <li><a href="{{route('products.checkout')}}">Check Out</a></li>
            <li><a href="{{route('posts.detail', ['id' => 1, 'slug' => 'post-samplate'])}}">Blog Details</a></li>
        </ul>
        </li> --}}
        <li><a href="{{ route('posts.index') }}">Tin tức</a></li>
        <li><a href="{{ route('contact') }}">Liên hệ</a></li>
        </ul>
    </nav>
    <div id="mobile-menu-wrap"></div>
    <div class="header__top__right__social">
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-linkedin"></i></a>
        <a href="#"><i class="fa fa-pinterest-p"></i></a>
    </div>
    <div class="humberger__menu__contact">
        <ul>
            <li><i class="fa fa-envelope"></i> admin@ogani.com</li>
            <li>Miễn khí vận chuyển cho đơn hàng trên 2200k</li>
        </ul>
    </div>
</div>


<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">
                        <ul>
                            <li><i class="fa fa-envelope"></i> admin@ogani.com</li>
                            <li>Miễn khí vận chuyển cho đơn hàng trên 2200k</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__right">
                        <div class="header__top__right__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        </div>
                        {{-- <div class="header__top__right__language">
                            <img src="https://preview.colorlib.com/theme/ogani/img/language.png" alt="">
                            <div>English</div>
                            <span class="arrow_carrot-down"></span>
                            <ul>
                                <li><a href="#">Spanis</a></li>
                                <li><a href="#">English</a></li>
                            </ul>
                        </div> --}}
                        @if (Auth::check())
                        <div class="header__top__right__auth">
                            <div>
                                <a href="{{route('my-account')}}">
                                    <img src="{{ env('MIX_S3_URL') . Auth::user()->avatar }}" alt="">
                                    Hi! {{ Auth::user()->name }}
                                </a>
                            </div>
                        </div>
                        @else
                        <div class="header__top__right__auth">
                            <a href="{{ route('user.login') }}">
                                <i class="fa fa-user"></i> Đăng nhập
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="header__logo">
                    <a href="{{ route('home') }}"><img src="https://preview.colorlib.com/theme/ogani/img/logo.png"
                            alt=""></a>
                </div>
            </div>
            <div class="col-lg-6">
                <nav class="header__menu">
                    <ul>
                        <li class="{{ $currentRouteName == 'home' ? 'active' : '' }}"><a
                                href="{{ route('home') }}">Trang chủ</a></li>
                        <li><a href="{{ route('products.index') }}">Cửa hàng</a></li>
                        {{-- <li><a href="#">Pages</a>
                            <ul class="header__menu__dropdown">
                                <li><a href="{{route('products.detail', ['id' => 1, 'slug' => 'product-example'])}}">Shop
                        Details</a></li>
                        <li><a href="{{route('products.cart')}}">Shoping Cart</a></li>
                        <li><a href="{{route('products.checkout')}}">Check Out</a></li>
                        <li><a href="{{route('posts.detail', ['id' => 1, 'slug' => 'post-samplate'])}}">Blog Details</a>
                        </li>
                    </ul>
                    </li> --}}
                    <li><a href="{{ route('posts.index') }}">Tin tức</a></li>
                    <li><a href="{{ route('contact') }}">Liên hệ</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3">
                <div class="header__cart">
                    <ul>
                        {{-- <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li> --}}
                        <li>
                            <a href="{{route('products.cart')}}">
                                <i class="fa fa-shopping-bag"></i>
                                <span>{{Cart::content()->count()}}</span>
                            </a>
                        </li>
                    </ul>
                    <div class="header__cart__price">Thành tiền: <span>{{Cart::total(0, 0, '.')}}đ</span></div>
                </div>
            </div>
        </div>
        <div class="humberger__open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>


<section class="hero {{ $currentRouteName != 'home' ? 'hero-normal' : '' }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="hero__categories">
                    <div class="hero__categories__all">
                        <i class="fa fa-bars"></i>
                        <span>Danh mục</span>
                    </div>
                    <ul>
                        @foreach ($categories as $category)
                        <li>
                            <a
                                href="{{route('products.category', ['id' => $category->id, 'slug' => $category->slug])}}">{{$category->name}}</a>
                        </li>
                        @endforeach
                        {{-- <li><a href="#">Rau củ quả</a></li>
                        <li><a href="#">Quà tặng</a></li>
                        <li><a href="#">Quả mọng</a></li>
                        <li><a href="#">Hải sản</a></li>
                        <li><a href="#">Bơ &amp; Trứng</a></li>
                        <li><a href="#">Đồ ăn nhanh</a></li>
                        <li><a href="#">Hành tươi</a></li>
                        <li><a href="#">Các món sấy</a></li>
                        <li><a href="#">Sữa tươi</a></li>
                        <li><a href="#">Chuối tươi</a></li> --}}
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="hero__search">
                    <div class="hero__search__form">
                        <form action="{{route('search.index')}}" method="GET">
                            {{-- <div class="hero__search__categories">
                                All Categories
                                <span class="arrow_carrot-down"></span>
                            </div> --}}
                            <input type="text" name="s" placeholder="Bạn muốn tìm gì?">
                            <button type="submit" class="site-btn">Tìm kiếm</button>
                        </form>
                    </div>
                    <div class="hero__search__phone">
                        <div class="hero__search__phone__icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="hero__search__phone__text">
                            <h5>+84 973.074.528</h5>
                            <span>hỗ trợ 24/7</span>
                        </div>
                    </div>
                </div>
                @if ($currentRouteName == 'home')
                <div class="hero__item set-bg" data-setbg="https://preview.colorlib.com/theme/ogani/img/hero/banner.jpg"
                    style="background-image: url(&quot;https://preview.colorlib.com/theme/ogani/img/hero/banner.jpg&quot;);">
                    <div class="hero__text">
                        <span>Hoa quả sạch</span>
                        <h2>Rau củ quả <br>100% hữu cơ</h2>
                        <p>Nhận và giao hàng miễn phí</p>
                        <a href="{{route('products.index')}}" class="primary-btn">Vào cửa hàng</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
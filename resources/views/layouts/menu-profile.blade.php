@php
    $currentRouteName = Route::currentRouteName();
@endphp
<div class="col-sm-4">
    <div class="list-group">
        <a href="{{route('my-account')}}" class="list-group-item list-group-item-action {{ $currentRouteName == 'my-account' ? 'active' : '' }}">
            Thông tin các nhân
        </a>
        <a href="{{route('order-history')}}" class="list-group-item list-group-item-action {{ $currentRouteName == 'order-history' ? 'active' : '' }}">Lịch sử đặt hàng</a>
        <a href="{{route('user.change-password')}}" class="list-group-item list-group-item-action {{ $currentRouteName == 'user.change-password' ? 'active' : '' }}">Đổi mật khẩu</a>
        <a href="{{route('user.logout')}}" class="list-group-item list-group-item-action ">Đăng xuất</a>
    </div>
</div>
@extends('master')

@section('content')
<div id="home_page">
    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Sản phẩm nổi bật</h2>
                    </div>
                    <div class="featured__controls">
                        <ul>
                            <li class="active" data-filter="*">Tất cả</li>
                            <li data-filter=".rau-cu-qua">Hoa quả</li>
                            <li data-filter=".thit-tuoi-song">Thịt tươi sống</li>
                            <li data-filter=".rau-cu-qua">Rau củ quả</li>
                            <li data-filter=".do-an-nhanh">Đồ ăn nhanh</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row featured__filter" id="MixItUp90B4A0">
                @foreach ($products as $item)
                <div
                    class="col-lg-3 col-md-4 col-sm-6 mix @foreach ($item->categories as $cat){{$cat->slug}} @endforeach">

                    <div class="featured__item">
                        <div class="featured__item__pic set-bg" data-setbg="{{Storage::url($item->image)}}">
                            <ul class="featured__item__pic__hover">
                                {{-- <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li> --}}
                                <li>
                                    <a href="{{route('carts.get.store', [$item->id])}}">
                                        <i class="fa fa-shopping-cart"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="{{route('products.detail', [$item->id, $item->slug])}}">{{$item->title}}</a>
                            </h6>
                            <h5>{{number_format($item->price, 0, ',', '.')}}đ</h5>
                        </div>
                    </div>

                </div>
                @endforeach

            </div>
        </div>
    </section>

    <section class="latest-product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="latest-product__text">
                        <h4>Mới nhất</h4>
                        <div class="latest-product__slider owl-carousel owl-loaded owl-drag">
                            @for ($i = 0; $i < count($featured); $i+=3) @php $p1=$products[$i]; $p2=$products[$i+1];
                                $p3=$products[$i+2]; @endphp <div class="latest-prdouct__slider__item">
                                <a href="{{route('products.detail', [$p1->id, $p1->slug])}}"
                                    class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{Storage::url($p1->image)}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>{{$p1->title}}</h6>
                                        <span>{{number_format($p1->price, 0, ',', '.')}}đ</span>
                                    </div>
                                </a>
                                <a href="{{route('products.detail', [$p2->id, $p2->slug])}}"
                                    class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{Storage::url($p2->image)}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>{{$p2->title}}</h6>
                                        <span>{{number_format($p2->price, 0, ',', '.')}}đ</span>
                                    </div>
                                </a>
                                <a href="{{route('products.detail', [$p3->id, $p3->slug])}}"
                                    class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{Storage::url($p3->image)}}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>{{$p3->title}}</h6>
                                        <span>{{number_format($p3->price, 0, ',', '.')}}đ</span>
                                    </div>
                                </a>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="latest-product__text">
                    <h4>Đánh giá cao</h4>
                    <div class="latest-product__slider owl-carousel owl-loaded owl-drag">
                        @for ($i = 0; $i < count($rate); $i+=3) @php $p1=isset($products[$i]) ? $products[$i] :null; $p2=isset($products[$i+1]) ? $products[$i+1] :null;
                            $p3=isset($products[$i+2]) ? $products[$i+2] :null; @endphp <div class="latest-prdouct__slider__item">
                            @if($p1)
                            <a href="{{route('products.detail', [$p1->id, $p1->slug])}}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{Storage::url($p1->image)}}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{$p1->title}}</h6>
                                    <span>{{number_format($p1->price, 0, ',', '.')}}đ</span>
                                </div>
                            </a>
                            @endif
                            @if($p2)
                            <a href="{{route('products.detail', [$p2->id, $p2->slug])}}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{Storage::url($p2->image)}}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{$p2->title}}</h6>
                                    <span>{{number_format($p2->price, 0, ',', '.')}}đ</span>
                                </div>
                            </a>
                            @endif
                            @if($p3)
                            <a href="{{route('products.detail', [$p3->id, $p3->slug])}}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{Storage::url($p3->image)}}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{$p3->title}}</h6>
                                    <span>{{number_format($p3->price, 0, ',', '.')}}đ</span>
                                </div>
                            </a>
                            @endif
                    </div>
                    @endfor
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="latest-product__text">
                <h4>Nổi bật</h4>
                <div class="latest-product__slider owl-carousel owl-loaded owl-drag">
                    @for ($i = 0; $i < count($recent); $i+=3) @php $p1=isset($products[$i]) ? $products[$i] :null; $p2=isset($products[$i+1]) ? $products[$i+1] :null;
                            $p3=isset($products[$i+2]) ? $products[$i+2] :null; @endphp <div class="latest-prdouct__slider__item">
                            @if($p1)
                            <a href="{{route('products.detail', [$p1->id, $p1->slug])}}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{Storage::url($p1->image)}}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{$p1->title}}</h6>
                                    <span>{{number_format($p1->price, 0, ',', '.')}}đ</span>
                                </div>
                            </a>
                            @endif
                            @if($p2)
                            <a href="{{route('products.detail', [$p2->id, $p2->slug])}}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{Storage::url($p2->image)}}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{$p2->title}}</h6>
                                    <span>{{number_format($p2->price, 0, ',', '.')}}đ</span>
                                </div>
                            </a>
                            @endif
                            @if($p3)
                            <a href="{{route('products.detail', [$p3->id, $p3->slug])}}" class="latest-product__item">
                                <div class="latest-product__item__pic">
                                    <img src="{{Storage::url($p3->image)}}" alt="">
                                </div>
                                <div class="latest-product__item__text">
                                    <h6>{{$p3->title}}</h6>
                                    <span>{{number_format($p3->price, 0, ',', '.')}}đ</span>
                                </div>
                            </a>
                            @endif
                    </div>
                @endfor
            </div>
        </div>
</div>
</div>
</div>
</section>


<section class="from-blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title from-blog__title">
                    <h2>Tin tức</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($posts as $item)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog__item">
                    <div class="blog__item__pic">
                        <a href="{{route('posts.detail', [$item->id, $item->slug])}}">
                                <img src="{{Storage::url($item->image)}}" alt="">
                        </a>
                    </div>
                    <div class="blog__item__text">
                        {{-- <ul>
                            <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                            <li><i class="fa fa-comment-o"></i> 5</li>
                        </ul> --}}
                        <h5><a href="{{route('posts.detail', [$item->id, $item->slug])}}">{{$item->title}}</a></h5>
                        <p>{{$item->excerpt}}</p>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
</div>
@endsection
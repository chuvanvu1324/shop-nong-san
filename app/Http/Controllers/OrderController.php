<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function detail()
    {
        return view('pages.orders.detail');
    }
}

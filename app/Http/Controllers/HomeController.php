<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Post;

class HOmeController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->take(3)->get();
        $products = Product::all();
        $featured = Product::where('featured', '=', '1')->get();
        $rate = Product::orderBy('rate', 'desc')->get();
        $recent = Product::orderBy('created_at', 'desc')->get();
        
        return view('home', compact('products', 'featured', 'rate', 'recent', 'posts'));
    }
}

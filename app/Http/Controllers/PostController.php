<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;

class PostController extends Controller
{
    public function index()
    {
        $categories = Category::post()->get();
        $recentPost = Post::orderBy('created_at', 'DESC')->take(3)->get();

        $posts = Post::paginate(12, ['*'], 'page')->withQueryString();
        return view('pages.posts.index', compact('categories', 'posts', 'recentPost'));
    }
    
    public function category($id, $slug)
    {
        $categories = Category::post()->get();
        $recentPost = Post::orderBy('created_at', 'DESC')->take(3)->get();

        $cat = Category::findOrFail($id);
        $posts = $cat->posts()->paginate(12, ['*'], 'page')->withQueryString();
        return view('pages.posts.index', compact('categories', 'posts', 'cat', 'recentPost'));
    }

    
    public function detail($id, $slug)
    {
        $categories = Category::post()->get();
        $recentPost = Post::orderBy('created_at', 'DESC')->take(3)->get();

        $post = Post::findOrFail($id);
        $relatedPosts = $post->relatedPosts()->take(3)->get();
        return view('pages.posts.detail', compact('post', 'categories', 'relatedPosts', 'recentPost'));
    }

}

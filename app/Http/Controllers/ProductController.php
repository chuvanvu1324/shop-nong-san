<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use Cart;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::product()->get();
        $saleOffs = Product::whereNotNull('old_price')->get()->take(10);
        $products = Product::paginate(12, ['*'], 'page')->withQueryString();
        return view('pages.products.index', compact('categories', 'saleOffs', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function detail($id, $slug) {
        $product = Product::findOrFail($id);
        $relatedProducts = $product->relatedProducts()->take(4)->get();
        return view('pages.products.detail', compact('product', 'relatedProducts'));
    }
    
    public function cart() {
        return view('pages.products.cart');
    }
    
    public function checkout() {
        return view('pages.products.checkout');
    }
    
    public function category($id) {
        $category = Category::findOrFail($id);
        //dd($category);
        $newProducts = Product::all()->take(9);
        $categories = Category::product()->get();
        $products = $category->products()->paginate(12, ['*'], 'page')->withQueryString();
        return view('pages.products.category', compact('categories', 'products', 'category', 'newProducts'));
    }



}

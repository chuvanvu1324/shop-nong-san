<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart, Auth;
use App\Models\Product;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $product = Product::findOrFail($id);
        $qty = 1;
        if(isset($request->qty)) {
            $qty = $request->qty;
        }
        $itemInCart = Cart::search(function ($cartItem, $rowId) use ($product) {
            // var_dump($product->id);
            return $cartItem->id === $product->id;
        });
        // Cart::destroy();
        // return;
        // var_dump(count($itemInCart));
        if (count($itemInCart) > 0) {
            $itemInCart = $itemInCart->values()[0];
            Cart::update($itemInCart->rowId, ['qty' => $itemInCart->qty+1]);
            // dd('da them cai nua');
        } else {
            Cart::add([
                    'id' => $product->id,
                    'name' => $product->title,
                    'qty' => $qty,
                    'price' => $product->price,
                    'weight' => 0,
                    'options' => ['image' => $product->image]
                    ]);
        }
        if(Auth::check()) {
            Cart::restore(Auth::user()->email);
            Cart::store(Auth::user()->email);
        }  
                
        return redirect()->route('products.cart');
    }

    public function getStore($product_id)
    {


        $id = $product_id;
        $product = Product::findOrFail($id);
        $qty = 1;
        if(isset($request->qty)) {
            $qty = $request->qty;
        }
        $itemInCart = Cart::search(function ($cartItem, $rowId) use ($product) {
            // var_dump($product->id);
            return $cartItem->id === $product->id;
        });
        // Cart::destroy();
        // return;
        // var_dump(count($itemInCart));
        if (count($itemInCart) > 0) {
            $itemInCart = $itemInCart->values()[0];
            Cart::update($itemInCart->rowId, ['qty' => $itemInCart->qty+1]);
            // dd('da them cai nua');
        } else {
            Cart::add([
                    'id' => $product->id,
                    'name' => $product->title,
                    'qty' => $qty,
                    'price' => $product->price,
                    'weight' => 0,
                    'options' => ['image' => $product->image]
                    ]);
        }
        if(Auth::check()) {
            Cart::erase(Auth::user()->email);
            Cart::store(Auth::user()->email);
        }  
        return redirect()->route('products.cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        foreach ($data as $key => $value) {
            // var_dump($key);
            Cart::update($key, ['qty' => $value]);
        }
        if(Auth::check()) {
            Cart::erase(Auth::user()->email);
            Cart::store(Auth::user()->email);
        }  
        return redirect()->route('products.cart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        if(Auth::check()) {
            Cart::erase(Auth::user()->email);
            Cart::store(Auth::user()->email);
        }  
        return redirect()->route('products.cart');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Post;
use App\Models\Category;

class SearchController extends Controller
{
    public function index(Request $req)
    {
        
        $s = $req->input('s');
        $products = Product::where('title', 'like', "%$s%")->get();
        $posts = Post::where('title', 'like', "%$s%")->get();
        return view('pages.search', compact('products', 'posts'));
    }
}

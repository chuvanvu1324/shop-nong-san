<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use TCG\Voyager\Http\Controllers\Controller as AtvController;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use Cart;

class AuthController extends AtvController
{
    use AuthenticatesUsers;

    public function login()
    {
        if ($this->guard()->user()) {
            return redirect()->route('home');
        }

        return view('pages.login');
    }

    public function postLogin(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            Cart::merge(Auth::user()->email);
            return redirect()->route('home');
            // return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    public function redirectTo()
    {
        return config('voyager.user.redirect', route('home'));
    }


    protected function guard()
    {
        return Auth::guard(app('VoyagerGuard'));
    }

}

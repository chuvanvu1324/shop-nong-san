<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use App\Models\UserMeta;
use TCG\Voyager\Models\Role;
use Auth;

class UserController extends Controller
{
    public function profile()
    {
        if (!Auth::check()) {
            return redirect()->route('home');
        }


        return view('pages.profile');
    }
    
    public function update(Request $req)
    {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        if(isset($req->name)) {
            $user->name = $req->name;
        }

        $metas = $user->userMetas()->get();
        foreach ($metas as $meta) {
           if(isset($req->all()[$meta->meta_key]))  {
            $meta->meta_value = $req->all()[$meta->meta_key];
            $meta->save();
           }
        }

        $user->save();

        return redirect()->route('my-account')->with(['status' => 'success', 'message' => 'Cập nhật thông tin thành công!']);
    }

    
    public function editPassword()
    {
        if (!Auth::check()) {
            return redirect()->route('home');
        }

        return view('pages.change-password');
    }
    
    public function orderHistory()
    {
        if (!Auth::check()) {
            return redirect()->route('home');
        }

        $orders = Order::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->get();
        return view('pages.order-history', compact('orders'));
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }
    
    public function register()
    {
        return view('pages.register');
    }
    
    public function postRegister(Request $req)
    {
        $foundUser = User::where('email', $req->email)->first();

        if($foundUser) {
            return redirect()->route('user.register')->with(['status' => 'danger', 'message' => 'Email này đã được đăng ký']);
        }
        $role = Role::where('name', 'user')->firstOrFail();
        $user = new User();
        $user->role_id = $role->id;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = bcrypt($req->password);
        $user->avatar = 'users/default.png';
        $user->save();

        $userMeta = new UserMeta();
        $userMeta->user_id = $user->id;
        $userMeta->meta_key = 'address';
        $userMeta->meta_value = $req->address;
        $userMeta->save();
        
        $userMeta = new UserMeta();
        $userMeta->user_id = $user->id;
        $userMeta->meta_key = 'phone';
        $userMeta->meta_value = $req->phone;
        $userMeta->save();

        Auth::login($user);
        
        return redirect()->route('home');
    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use Cart;
use Auth;

class CheckoutController extends Controller
{
    public function index()
    {
        return view('pages.products.checkout');
    }
    
    public function store(Request $req)
    {
        $order = new Order();

        $order->user_id = Auth::check() ? Auth::user()->id : null;
        $order->qty = Cart::count();
        $order->total =  Cart::total(0, '', '');
        $order->note = $req->note;
        $order->status = 0;
        $order->payment_method = 'cod';
        $order->save();


        foreach (Cart::content() as $row) {
            $detail = new OrderDetail();
            $detail->product_id = $row->id;
            $detail->qty = $row->qty;
            $detail->order_id = $order->id;
            $detail->save();
        }

        Cart::destroy();
        if(Auth::check()) {

            return redirect()->route('order-history')->with(['status' => 'success', 'message' => 'Đặt hàng thành công']);
        } else {
            return redirect()->route('products.index')->with(['status' => 'success', 'message' => 'Đặt hàng thành công']);
        }
    }
}

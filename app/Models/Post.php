<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use  App\Models\PostCategory;

class Post extends Model
{
    use HasFactory;

    public function relatedPosts()
    {
        $cat_ids = [];
        $categories = $this->belongsToMany(Category::class, PostCategory::class)->get();
        foreach ($categories as $value) {
            $cat_ids[] = $value->id;
        }
        $data = PostCategory::whereIn('post_categories.category_id', $cat_ids)->whereNotIn('post_categories.post_id', [$this->id])->join('posts', 'posts.id', '=', 'post_categories.post_id')
        ->select('posts.*')->distinct();

        return $data;
    }
}

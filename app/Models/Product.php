<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    use Translatable;
    use HasFactory;

    protected $translatable = ['title', 'seo_title', 'excerpt', 'body', 'slug', 'meta_description', 'meta_keywords'];

    public const PUBLISHED = 'PUBLISHED';

    protected $guarded = [];

    public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the product
        if (!$this->author_id && Auth::user()) {
            $this->author_id = Auth::user()->getKey();
        }

        return parent::save();
    }

    public function authorId()
    {
        return $this->belongsTo(Voyager::modelClass('User'), 'author_id', 'id');
    }

    /**
     * Scope a query to only published scopes.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::PUBLISHED);
    }
    
    public function scopeInstock(Builder $query)
    {
        return $query->where('status', '=', 'instock');
    }
    
    public function categories() {
        return $this->belongsToMany(Category::class, ProductCategory::class);
    }

    public function relatedProducts()
    {
        $cat_ids = [];
        $categories = $this->belongsToMany(Category::class, ProductCategory::class)->get();
        foreach ($categories as $value) {
            $cat_ids[] = $value->id;
        }
        $data = ProductCategory::whereIn('product_categories.category_id', $cat_ids)->whereNotIn('product_categories.product_id', [$this->id])->join('products', 'products.id', '=', 'product_categories.product_id')
        ->select('products.*')->distinct();

        return $data;
    }

}

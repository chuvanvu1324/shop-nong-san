<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::group(['prefix' => 'user'], function () {
    Route::get('/login', 'AuthController@login')->name('user.login');
    Route::post('/login', 'AuthController@postLogin')->name('user.postLogin');
    Route::post('/update', 'UserController@update')->name('user.update');
    Route::get('/change-password', 'UserController@editPassword')->name('user.change-password');
    Route::post('/change-password', 'UserController@updatePassword')->name('user.update-password');
    Route::get('/logout', 'UserController@logout')->name('user.logout');
    Route::get('/register', 'UserController@register')->name('user.register');
    Route::post('/register', 'UserController@postRegister')->name('user.postRegister');
});


Route::group(['prefix' => 'posts'], function () {
    Route::get('/', 'PostController@index')->name('posts.index');
    Route::get('/{id}/{slug}', 'PostController@detail')->name('posts.detail');
    Route::get('cat/{id}/{slug}', 'PostController@category')->name('posts.category');
});

Route::group(['prefix' => 'products'], function () {
    Route::resource('/', ProductController::class, [
        'names' => [
            'index' => 'products.index'
        ]
    ]);
    Route::get('/cart', 'ProductController@cart')->name('products.cart');
    Route::get('/checkout', 'ProductController@checkout')->name('products.checkout');
    Route::get('/cat/{id}/{slug}', 'ProductController@category')->name('products.category');
    Route::get('/{id}/{slug}', 'ProductController@detail')->name('products.detail');
});

Route::group(['prefix' => 'carts'], function () {
    Route::get('/{product_id}/store', 'CartController@getStore')->name('carts.get.store');
    Route::post('/', 'CartController@store')->name('carts.store');
    Route::get('/{id}/delete', 'CartController@destroy')->name('carts.destroy');
    Route::post('/update', 'CartController@update')->name('carts.update');
    // Route::delete('/{id}', 'CartController@destroy')->name('carts.destroy');
});

Route::group(['prefix' => 'checkout'], function () {
    Route::get('/', 'CheckoutController@index')->name('checkout.index');
    Route::post('/', 'CheckoutController@store')->name('checkout.store');

});

Route::group(['prefix' => 'order'], function () {
    Route::get('/{id}/detail', 'OrderController@detail')->name('orders.detail');

});




Route::get('/my-account', 'UserController@profile')->name('my-account');
Route::get('/order-history', 'UserController@orderHistory')->name('order-history');
Route::group(['prefix' => 'change-password'], function () {
    
    Route::get('/', 'UserController@getChangePassword')->name('getChangePassword');
    Route::post('/', 'UserController@postChangePassword')->name('postChangePassword');

});
Route::get('/contact', 'ContactController@index')->name('contact');

Route::group(['prefix' => 'search'], function () {
    Route::get('/', 'SearchController@index')->name('search.index');

});
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2021 at 05:35 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop_nong_san`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `image`, `category_type`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', NULL, 'post', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(2, NULL, 1, 'Category 2', 'category-2', NULL, 'post', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(3, NULL, 1, 'Thịt tươi sống', 'thit-tuoi-song', NULL, 'product', '2021-05-04 00:47:32', '2021-05-04 05:46:01'),
(4, NULL, 2, 'Rau củ quả', 'rau-cu-qua', NULL, 'product', '2021-05-04 00:47:32', '2021-05-04 05:46:26'),
(5, NULL, 3, 'Quà tặng', 'qua-tang', NULL, 'product', '2021-05-04 05:48:30', '2021-05-04 05:48:30'),
(6, NULL, 4, 'Quả mọng', 'qua-mong', NULL, 'product', '2021-05-04 05:48:48', '2021-05-04 05:48:48'),
(7, NULL, 5, 'Hải sản', 'hai-san', NULL, 'product', '2021-05-04 05:49:11', '2021-05-04 05:49:11'),
(8, NULL, 6, 'Bơ & trứng', 'bo-and-trung', NULL, 'product', '2021-05-04 05:49:28', '2021-05-04 05:49:28'),
(9, NULL, 7, 'Đồ ăn nhanh', 'do-an-nhanh', NULL, 'product', '2021-05-04 05:49:41', '2021-05-04 09:06:44'),
(10, NULL, 8, 'Hành tươi', 'hanh-tuoi', NULL, 'product', '2021-05-04 05:49:55', '2021-05-04 05:49:55'),
(11, NULL, 9, 'Các món sấy', 'cac-mon-say', NULL, 'product', '2021-05-04 05:50:08', '2021-05-04 05:50:08'),
(12, NULL, 10, 'Sữa tươi', 'sua-tuoi', NULL, 'product', '2021-05-04 05:50:24', '2021-05-04 05:50:24'),
(13, NULL, 11, 'Chuối tươi', 'chuoi-tuoi', NULL, 'product', '2021-05-04 05:50:35', '2021-05-04 05:50:35');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 5),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 7),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 8),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 5, 'post_belongstomany_category_relationship', 'relationship', 'categories', 0, 1, 1, 1, 1, 1, '{\"scope\":\"post\",\"model\":\"App\\\\Models\\\\Category\",\"table\":\"categories\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"post_categories\",\"pivot\":\"1\",\"taggable\":null}', 16),
(57, 7, 'product_belongstomany_category_relationship', 'relationship', 'categories', 0, 1, 1, 1, 1, 1, '{\"scope\":\"product\",\"model\":\"App\\\\Models\\\\Category\",\"table\":\"categories\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"product_categories\",\"pivot\":\"1\",\"taggable\":\"0\"}', 16),
(58, 8, 'user_meta_hasone_user_relationship', 'relationship', 'User', 1, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":null}', 16),
(59, 4, 'category_type', 'select_dropdown', 'Category Type', 1, 1, 1, 1, 1, 1, '{\"default\":\"post\",\"options\":{\"post\":\"post\",\"product\":\"product\"}}', 4),
(60, 7, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(61, 7, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '{}', 2),
(62, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 4),
(63, 7, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '{}', 5),
(64, 7, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '{}', 6),
(65, 7, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(66, 7, 'images', 'multiple_images', 'Images', 0, 0, 1, 1, 1, 1, '{}', 7),
(67, 7, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:products,slug\"}}', 8),
(68, 7, 'meta_description', 'text_area', 'Meta Description', 0, 0, 1, 1, 1, 1, '{}', 9),
(69, 7, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"instock\",\"options\":{\"instock\":\"instock\",\"outstock\":\"outstock\"}}', 11),
(70, 7, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 12),
(71, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(72, 7, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, '{}', 14),
(73, 7, 'featured', 'checkbox', 'Featured', 1, 0, 1, 1, 1, 1, '{}', 15),
(74, 7, 'price', 'number', 'Price', 1, 1, 1, 1, 1, 1, '{}', 15),
(75, 7, 'old_price', 'number', 'Old Price', 0, 0, 1, 1, 1, 1, '{}', 15),
(76, 7, 'rate', 'number', 'Rate', 1, 1, 1, 1, 1, 1, '{}', 15),
(77, 8, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(78, 8, 'user_id', 'number', 'User', 1, 1, 1, 1, 1, 1, NULL, 2),
(79, 8, 'meta_key', 'text', 'Meta Key', 1, 1, 1, 1, 1, 1, NULL, 3),
(80, 8, 'meta_value', 'text', 'Meta Value', 1, 1, 1, 1, 1, 1, NULL, 3),
(81, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 12),
(82, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 13),
(83, 4, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 6);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'App\\Http\\Controllers\\Admin\\UserController', '', 1, 0, NULL, '2021-05-04 00:47:31', '2021-05-04 00:47:32'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-05-04 00:47:31', '2021-05-04 05:48:17'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(7, 'products', 'products', 'Product', 'Products', 'voyager-shop', 'App\\Models\\Product', 'App\\Policies\\ProductPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-05-04 00:47:32', '2021-05-04 08:32:46'),
(8, 'user_metas', 'user-metas', 'User Meta', 'User Metas', 'voyager-list-add', 'App\\Models\\UserMeta', 'App\\Policies\\UserMetaPolicy', 'App\\Http\\Controllers\\Admin\\UserMetaController', '', 1, 0, NULL, '2021-05-04 00:47:32', '2021-05-04 00:47:32');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-05-04 00:47:31', '2021-05-04 00:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2021-05-04 00:47:31', '2021-05-04 00:47:31', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 8, '2021-05-04 00:47:31', '2021-05-04 00:47:31', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2021-05-04 00:47:32', '2021-05-04 00:47:32', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2021-05-04 00:47:32', '2021-05-04 00:47:32', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2021-05-04 00:47:32', '2021-05-04 00:47:32', 'voyager.hooks', NULL),
(15, 1, 'Products', '', '_self', 'voyager-shop', NULL, NULL, 6, '2021-05-04 00:47:32', '2021-05-04 00:47:32', 'voyager.products.index', NULL),
(16, 1, 'User Meta', '', '_self', 'voyager-list-add', NULL, NULL, 6, '2021-05-04 00:47:33', '2021-05-04 00:47:33', 'voyager.user-metas.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2019_08_19_000000_create_failed_jobs_table', 1),
(28, '2021_04_25_150145_create_post_categories_table', 1),
(29, '2021_04_27_032326_add_column_category_type_to_categories_table', 1),
(30, '2021_04_27_094103_create_products_table', 1),
(31, '2021_04_27_094643_create_product_categories_table', 1),
(32, '2021_04_27_163714_create_user_metas_table', 1),
(34, '2021_05_04_124205_add_image_to_category_table', 2),
(35, '2021_05_04_152956_change_column_status_product_table', 3),
(36, '2018_12_23_120000_create_shoppingcart_table', 4),
(39, '2021_05_07_154344_create_orders_table', 5),
(40, '2021_05_07_155510_create_order_detail_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `total` double NOT NULL,
  `status` int(11) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `qty`, `total`, `status`, `payment_method`, `note`, `created_at`, `updated_at`) VALUES
(1, NULL, 2, 198000000, 0, 'cod', 'sdfsdf', '2021-05-07 09:13:49', '2021-05-07 09:13:49'),
(2, NULL, 2, 198, 0, 'cod', 'sdfsdf', '2021-05-07 09:15:59', '2021-05-07 09:15:59'),
(3, 1, 5, 538, 0, 'cod', 'ddfd', '2021-05-07 09:17:20', '2021-05-07 09:17:20'),
(4, 1, 5, 538, 0, 'cod', 'ddfd', '2021-05-07 09:17:48', '2021-05-07 09:17:48'),
(5, 1, 32, 3211000, 0, 'cod', NULL, '2021-05-07 09:24:29', '2021-05-07 09:24:29'),
(6, 1, 141, 14159000, 0, 'cod', NULL, '2021-05-08 12:47:26', '2021-05-08 12:47:26'),
(7, 1, 1, 99000, 0, 'cod', NULL, '2021-05-08 12:48:30', '2021-05-08 12:48:30'),
(8, 1, 1, 100000, 0, 'cod', NULL, '2021-05-08 12:49:19', '2021-05-08 12:49:19'),
(9, NULL, 1, 135000, 0, 'cod', 'sf', '2021-05-08 12:55:59', '2021-05-08 12:55:59'),
(10, NULL, 1, 99000, 0, 'cod', NULL, '2021-05-08 12:57:19', '2021-05-08 12:57:19'),
(11, NULL, 1, 141000, 0, 'cod', NULL, '2021-05-08 12:58:23', '2021-05-08 12:58:23');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`product_id`, `order_id`, `qty`, `created_at`, `updated_at`) VALUES
(3, 4, 1, '2021-05-07 09:17:48', '2021-05-07 09:17:48'),
(3, 5, 1, '2021-05-07 09:24:29', '2021-05-07 09:24:29'),
(3, 6, 100, '2021-05-08 12:47:26', '2021-05-08 12:47:26'),
(3, 8, 1, '2021-05-08 12:49:19', '2021-05-08 12:49:19'),
(4, 4, 3, '2021-05-07 09:17:48', '2021-05-07 09:17:48'),
(4, 5, 30, '2021-05-07 09:24:29', '2021-05-07 09:24:29'),
(4, 6, 11, '2021-05-08 12:47:26', '2021-05-08 12:47:26'),
(4, 7, 1, '2021-05-08 12:48:30', '2021-05-08 12:48:30'),
(4, 10, 1, '2021-05-08 12:57:19', '2021-05-08 12:57:19'),
(5, 4, 1, '2021-05-07 09:17:48', '2021-05-07 09:17:48'),
(5, 5, 1, '2021-05-07 09:24:29', '2021-05-07 09:24:29'),
(5, 11, 1, '2021-05-08 12:58:23', '2021-05-08 12:58:23'),
(6, 6, 2, '2021-05-08 12:47:26', '2021-05-08 12:47:26'),
(6, 9, 1, '2021-05-08 12:55:59', '2021-05-08 12:55:59'),
(8, 6, 28, '2021-05-08 12:47:26', '2021-05-08 12:47:26');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2021-05-04 00:47:32', '2021-05-04 00:47:32');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(2, 'browse_bread', NULL, '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(3, 'browse_database', NULL, '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(4, 'browse_media', NULL, '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(5, 'browse_compass', NULL, '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(6, 'browse_menus', 'menus', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(7, 'read_menus', 'menus', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(8, 'edit_menus', 'menus', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(9, 'add_menus', 'menus', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(10, 'delete_menus', 'menus', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(11, 'browse_roles', 'roles', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(12, 'read_roles', 'roles', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(13, 'edit_roles', 'roles', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(14, 'add_roles', 'roles', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(15, 'delete_roles', 'roles', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(16, 'browse_users', 'users', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(17, 'read_users', 'users', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(18, 'edit_users', 'users', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(19, 'add_users', 'users', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(20, 'delete_users', 'users', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(21, 'browse_settings', 'settings', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(22, 'read_settings', 'settings', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(23, 'edit_settings', 'settings', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(24, 'add_settings', 'settings', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(25, 'delete_settings', 'settings', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(26, 'browse_categories', 'categories', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(27, 'read_categories', 'categories', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(28, 'edit_categories', 'categories', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(29, 'add_categories', 'categories', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(30, 'delete_categories', 'categories', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(31, 'browse_posts', 'posts', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(32, 'read_posts', 'posts', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(33, 'edit_posts', 'posts', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(34, 'add_posts', 'posts', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(35, 'delete_posts', 'posts', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(36, 'browse_pages', 'pages', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(37, 'read_pages', 'pages', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(38, 'edit_pages', 'pages', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(39, 'add_pages', 'pages', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(40, 'delete_pages', 'pages', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(41, 'browse_hooks', NULL, '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(42, 'browse_product', NULL, '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(43, 'browse_user_meta', NULL, '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(44, 'browse_products', 'products', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(45, 'read_products', 'products', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(46, 'edit_products', 'products', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(47, 'add_products', 'products', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(48, 'delete_products', 'products', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(49, 'browse_user_metas', 'user_metas', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(50, 'read_user_metas', 'user_metas', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(51, 'edit_user_metas', 'user_metas', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(52, 'add_user_metas', 'user_metas', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(53, 'delete_user_metas', 'user_metas', '2021-05-04 00:47:33', '2021-05-04 00:47:33');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-05-04 00:47:32', '2021-05-04 00:47:32');

-- --------------------------------------------------------

--
-- Table structure for table `post_categories`
--

CREATE TABLE `post_categories` (
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `old_price` double DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` double NOT NULL DEFAULT 5,
  `seo_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'instock',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `author_id`, `title`, `slug`, `price`, `old_price`, `excerpt`, `body`, `rate`, `seo_title`, `image`, `images`, `meta_description`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(3, 1, 'Hoa quả sấy khô', 'hoa-qua-say-kho', 100000, 120000, 'Với một lượng trái cây sấy khô thích hợp mỗi ngày, cơ thể sẽ được bổ sung thêm lượng vitamin và các dưỡng chất dồi dào để giúp bạn luôn tươi trẻ và khỏe mạnh', '<p>Với một lượng tr&aacute;i c&acirc;y sấy kh&ocirc; th&iacute;ch hợp mỗi ng&agrave;y, cơ thể sẽ được bổ sung th&ecirc;m lượng vitamin v&agrave; c&aacute;c dưỡng chất dồi d&agrave;o để gi&uacute;p bạn lu&ocirc;n tươi trẻ v&agrave; khỏe mạnh</p>', 4, NULL, 'products\\May2021\\1evYbb0r87tnR1OZHwFS.jpg', NULL, NULL, 'instock', 1, '2021-05-04 08:57:39', '2021-05-08 11:27:12'),
(4, 1, 'Rau củ quả', 'rau-cu-qua', 99000, 70000, 'Các loại rau củ quả vốn được biết đến là rất tốt cho sức khỏe do chứa ít calo mà lại giàu vitamin, các khoáng chất và chất x', '<p><span style=\"color: #4d5156; font-family: arial, sans-serif;\">C&aacute;c loại&nbsp;</span><span style=\"font-weight: bold; color: #5f6368; font-family: arial, sans-serif;\">rau củ quả</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">&nbsp;vốn được biết đến l&agrave; rất tốt cho sức khỏe do chứa &iacute;t calo m&agrave; lại gi&agrave;u vitamin, c&aacute;c kho&aacute;ng chất v&agrave; chất x</span></p>', 5, NULL, 'products\\May2021\\UHrOTCZ5BxWYaJFbbyry.jpg', NULL, NULL, 'instock', 1, '2021-05-04 09:00:22', '2021-05-04 09:00:22'),
(5, 1, 'Trái cây hỗn hợp', 'trai-cay-hon-hop', 141000, 120000, 'Trái cây khô hỗn hợp là nguyên liệu làm bánh được sử dụng rộng rãi trong các món bánh, có mùi vị thơm ngon và giàu chất dinh dưỡng, đảm bảo vệ sinh an', '<p><span style=\"font-weight: bold; color: #5f6368; font-family: arial, sans-serif;\">Tr&aacute;i c&acirc;y</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">&nbsp;kh&ocirc;&nbsp;</span><span style=\"font-weight: bold; color: #5f6368; font-family: arial, sans-serif;\">hỗn hợp</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">&nbsp;l&agrave; nguy&ecirc;n liệu l&agrave;m b&aacute;nh được sử dụng rộng r&atilde;i trong c&aacute;c m&oacute;n b&aacute;nh, c&oacute; m&ugrave;i vị thơm ngon v&agrave; gi&agrave;u chất dinh dưỡng, đảm bảo vệ sinh an</span></p>', 5, NULL, 'products\\May2021\\t9gTygUPiLFgniil2chT.jpg', NULL, NULL, 'instock', 1, '2021-05-04 09:03:20', '2021-05-04 09:03:20'),
(6, 1, 'Nho khô và các loại hạt', 'nho-kho-va-cac-loai-hat', 135000, 125000, 'Nho khô loại nào ngon? — Theo các chuyên gia, nho khô ngon nhất thiết phải được làm từ nho ... Ngoài vấn đề xuất xứ, để nhận diện nho khô loại nào ngon, bạn ... Nếu nho rời ra từng hạt tức là nho đã được sấy đúng chuẩn.', '<p><span class=\"f\" style=\"color: #70757a; line-height: 1.58; font-family: arial, sans-serif;\"><strong>Nho kh&ocirc; loại</strong>&nbsp;n&agrave;o ngon? &mdash;&nbsp;</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">Theo&nbsp;<span style=\"font-weight: bold; color: #5f6368;\">c&aacute;c</span>&nbsp;chuy&ecirc;n gia,&nbsp;<span style=\"font-weight: bold; color: #5f6368;\">nho kh&ocirc;</span>&nbsp;ngon nhất thiết phải được l&agrave;m từ nho ... Ngo&agrave;i vấn đề xuất xứ, để nhận diện&nbsp;<span style=\"font-weight: bold; color: #5f6368;\">nho kh&ocirc; loại</span>&nbsp;n&agrave;o ngon, bạn ... Nếu nho rời ra từng&nbsp;<span style=\"font-weight: bold; color: #5f6368;\">hạt</span>&nbsp;tức l&agrave; nho đ&atilde; được sấy đ&uacute;ng chuẩn.</span></p>', 5, NULL, 'products\\May2021\\dqBSGMdIa7LGh0WDcXe3.jpg', NULL, NULL, 'instock', 1, '2021-05-04 09:05:21', '2021-05-04 09:05:21'),
(7, 1, 'ham bơ gơ', 'ham-bo-go', 58000, 50000, 'Hamburger hoặc gọi ngắn gọn là burger (Tiếng Việt gọi là hăm-bơ-gơ hay hem-bơ-gơ, phát âm tiếng Anh là /ˈhæmbɜrɡər/) là một loại thức ăn bao gồm bán', '<p><span style=\"color: #4d5156; font-family: arial, sans-serif;\">Hamburger hoặc gọi ngắn gọn l&agrave; burger (Tiếng Việt gọi l&agrave;&nbsp;</span><span style=\"font-weight: bold; color: #5f6368; font-family: arial, sans-serif;\">hăm</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">-</span><span style=\"font-weight: bold; color: #5f6368; font-family: arial, sans-serif;\">bơ-gơ</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">&nbsp;hay hem-</span><wbr style=\"color: #4d5156; font-family: arial, sans-serif;\" /><span style=\"color: #4d5156; font-family: arial, sans-serif;\">bơ-gơ, ph&aacute;t &acirc;m tiếng Anh l&agrave; /ˈh&aelig;mbɜrɡər/) l&agrave; một loại thức ăn bao gồm b&aacute;n</span></p>', 5, NULL, 'products\\May2021\\RnGQeArUgGTEjfsDX4Wl.jpg', NULL, NULL, 'instock', 0, '2021-05-04 09:07:31', '2021-05-04 09:07:31'),
(8, 1, 'Nho', 'nho', 100000, 50000, 'Theo các chuyên gia, nho khô ngon nhất thiết phải được làm từ nho ... Ngoài vấn đề xuất xứ, để nhận diện nho khô loại nào ngon, bạn ... Nếu nho rời ra từng hạt tức là nho đã được sấy đúng chuẩn.', '<p><span style=\"color: #4d5156; font-family: arial, sans-serif;\">Theo&nbsp;</span><span style=\"font-weight: bold; color: #5f6368; font-family: arial, sans-serif;\">c&aacute;c</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">&nbsp;chuy&ecirc;n gia,&nbsp;</span><span style=\"font-weight: bold; color: #5f6368; font-family: arial, sans-serif;\">nho kh&ocirc;</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">&nbsp;ngon nhất thiết phải được l&agrave;m từ nho ... Ngo&agrave;i vấn đề xuất xứ, để nhận diện&nbsp;</span><span style=\"font-weight: bold; color: #5f6368; font-family: arial, sans-serif;\">nho kh&ocirc; loại</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">&nbsp;n&agrave;o ngon, bạn ... Nếu nho rời ra từng&nbsp;</span><span style=\"font-weight: bold; color: #5f6368; font-family: arial, sans-serif;\">hạt</span><span style=\"color: #4d5156; font-family: arial, sans-serif;\">&nbsp;tức l&agrave; nho đ&atilde; được sấy đ&uacute;ng chuẩn.</span></p>', 5, NULL, 'products\\May2021\\zr0tTbs5lHnm9DAE58Pw.jpg', NULL, NULL, 'instock', 0, '2021-05-04 09:08:35', '2021-05-04 09:27:04');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`product_id`, `category_id`, `created_at`, `updated_at`) VALUES
(3, 4, NULL, NULL),
(3, 11, NULL, NULL),
(4, 4, NULL, NULL),
(5, 4, NULL, NULL),
(5, 11, NULL, NULL),
(6, 6, NULL, NULL),
(6, 11, NULL, NULL),
(7, 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-05-04 00:47:31', '2021-05-04 00:47:31'),
(2, 'user', 'Normal User', '2021-05-04 00:47:31', '2021-05-04 00:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Ogani', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Chào mừng đến với nông sản Thái nguyên', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(11, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2021-05-04 00:47:32', '2021-05-04 00:47:32'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2021-05-04 00:47:32', '2021-05-04 00:47:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$N8MgjqLsFCDlK81l4i5WOe3ZCOMoNdOzl2X/GnHy27koQHqE7MhZS', 'lwsUdoJM7bszriUHiSqDMYS5FSqxk1mtBA5srM2Wx6DGqZ0j7ZIccq9gAnnb', '{\"locale\":\"en\"}', '2021-05-04 00:47:32', '2021-05-08 01:26:20'),
(2, 2, 'User', 'user@gmail.com', 'users/default.png', NULL, '$2y$10$NkHNfQTK4P.B/1iWe2dfz.9V6hwBwWxZME8sW/r4Fz2rKE4dE8Zmm', NULL, '{\"locale\":\"en\"}', '2021-05-08 08:20:03', '2021-05-08 08:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `user_metas`
--

CREATE TABLE `user_metas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_metas`
--

INSERT INTO `user_metas` (`id`, `user_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(1, 1, 'phone', '097307452', '2021-05-04 00:47:33', '2021-05-08 01:32:33'),
(2, 1, 'address', '27 ngõ 20 Nguyễn Chánh - Trung Hòa - Cầu Giấy - HN', '2021-05-04 00:47:33', '2021-05-04 00:47:33'),
(3, 2, 'address', 'Tân Tinh - Thái Nguyên', '2021-05-08 08:20:03', '2021-05-08 08:24:14'),
(4, 2, 'phone', '0973173508', '2021-05-08 08:20:03', '2021-05-08 08:25:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`product_id`,`order_id`),
  ADD KEY `order_details_order_id_foreign` (`order_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `post_categories`
--
ALTER TABLE `post_categories`
  ADD PRIMARY KEY (`post_id`,`category_id`),
  ADD KEY `post_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `product_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_metas`
--
ALTER TABLE `user_metas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_metas_user_id_meta_key_unique` (`user_id`,`meta_key`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_metas`
--
ALTER TABLE `user_metas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_details_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_categories`
--
ALTER TABLE `post_categories`
  ADD CONSTRAINT `post_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_categories_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD CONSTRAINT `product_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_categories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_metas`
--
ALTER TABLE `user_metas`
  ADD CONSTRAINT `user_metas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
